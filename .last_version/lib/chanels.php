<?php
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.push
 * @copyright  2014 Zahalski Andrew
 */

namespace Mlife\Push;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class ChanelsTable extends Entity\DataManager
{
	
	public static function getFilePath(){
		return __FILE__;
	}

	public static function getTableName(){
		return 'mlife_push_chanels';
	}
	
	public static function getMap(){
		return array(
			'ID' => array(
				'data_type' => 'string',
				'primary' => true,
				'autocomplete' => false,
				'title' => Loc::getMessage('MLIFE_PUSH_CHANELS_ENTITY_ID_FIELD'),
			),
			'USER' => array(
				'data_type' => 'integer',
				'required' => false,
				'title' => Loc::getMessage('MLIFE_PUSH_CHANELS_ENTITY_USER_FIELD'),
			),
			'TIMEEND' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('MLIFE_PUSH_CHANELS_ENTITY_TIMEEND_FIELD'),
			),
			'TYPE' => array(
				'data_type' => 'string',
				'required' => true,
				'title' => Loc::getMessage('MLIFE_PUSH_CHANELS_ENTITY_TYPE_FIELD'),
			),
		);
	}
	
	public static function getChanel($userId,$channelType="private"){
		
		$arResult = false;
		
		$userId = intval($userId);
		$cache_id = "chanel_".$userId.'_'.$channelType;
		$cache_dir = "/mlife_push_chanels/";
		$cache_time = "43200";
		
		$obCache = \Bitrix\Main\Data\Cache::createInstance();
		
		if( $obCache->initCache($cache_time,$cache_id,$cache_dir) ){
			$vars = $obCache->GetVars();
		}elseif( $obCache->startDataCache()){
			$vars = false;
			$arFields = array(
				"ID" => self::createRandChanel(),
				"USER" => $userId,
				"TIMEEND" => time()+$cache_time,
				"TYPE" => $channelType
			);
			$res = \Mlife\Push\ChanelsTable::add($arFields);
			if($res->isSuccess())
				$vars = $arFields["ID"];
				
			$obCache->endDataCache($vars);
		}
		$arResult = $vars;
		
		return $arResult;
		
	}
	
	public static function createRandChanel(){
		
		global $APPLICATION;
		return md5(uniqid().$_SERVER["REMOTE_ADDR"].$_SERVER["SERVER_NAME"].(is_object($APPLICATION)? $APPLICATION->GetServerUniqID(): ''));
		
	}
	
	public static function sendMessageUser($userId, $mess, $module="default", $comand="default"){
		
		$res = \Mlife\Push\ChanelsTable::getList(array(
			"select" => array("ID"),
			"filter" => array("USER" => $userId, ">TIMEEND" => time(), "TYPE" => "private"),
			"limit" => 1,
			"order" => array("TIMEEND"=>"DESC")
		));
		if($arData = $res->Fetch()){
			self::postToChanel($arData["ID"], $mess);
		}
		
	}
	
	public static function postToChanel($chanelId, $mess){
		
		$urlConf = \COption::GetOptionString("mlife.push", "url", "");
		
		if(!$urlConf) return false;
		
		$url = str_replace("#CHANEL_ID#",$chanelId,$urlConf);
		
		$http = new \Bitrix\Main\Web\HttpClient();
		$res = $http->post($url,$mess);
		return $res;
		
	}
	
	public static function agentDeleteDieChanels(){
		
		$entity = \Mlife\Push\ChanelsTable::getEntity();
		
		$connection = \Bitrix\Main\Application::getConnection();
		$helper = $connection->getSqlHelper();

		$tableName = $entity->getDBTableName();

		$sql = "DELETE FROM ".$tableName." WHERE TIMEEND < ".time();
		
		$connection->queryExecute($sql);
		
		return "\\Mlife\\Push\\ChanelsTable::agentDeleteDieChanels();";
		
	}
	
}