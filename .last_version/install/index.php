<?
IncludeModuleLangFile(__FILE__);

class mlife_push extends CModule
{
	var $MODULE_ID = "mlife.push";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;

	function mlife_push()
	{
		$arModuleVersion = array();
		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");

		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->PARTNER_NAME = GetMessage("MLIFE_COMPANY_NAME");
		$this->PARTNER_URI = "http://mlife-media.by/";
		$this->MODULE_NAME = GetMessage("MLIFE_PUSH_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("MLIFE_PUSH_DESCRIPTION");
		return true;
	}
	
	function InstallDB($arParams = array())
	{
		global $DB, $DBType, $APPLICATION;
		$this->errors = false;
		// Database tables creation
		
		$this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mlife.push/install/db/".strtolower($DB->type)."/install.sql");
		
		if ($this->errors !== false)
		{
			$APPLICATION->ThrowException(implode("<br>", $this->errors));
			return false;
		}
		else
		{
			RegisterModule("mlife.push");
		}
		return true;
	}
	
	function UnInstallDB($arParams = array())
	{
		global $DB, $DBType, $APPLICATION;
		$this->errors = false;

		// remove hl system data
		$this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mlife.push/install/db/".strtolower($DB->type)."/uninstall.sql");

		UnRegisterModule("mlife.push");

		if ($this->errors !== false)
		{
			$APPLICATION->ThrowException(implode("<br>", $this->errors));
			return false;
		}
		return true;
	}
	
	function InstallEvents()
	{	
		return true;
	}
	
	function UnInstallEvents()
	{
		return true;
	}
	
	function InstallFiles($arParams = array())
	{
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mlife.push/install/tools/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tools", true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mlife.push/install/components/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/components", true, true);
		return true;
	}
	
	function UnInstallFiles()
	{
		DeleteDirFilesEx("/bitrix/tools/mlife.push");
		DeleteDirFilesEx("/bitrix/components/mlife/mlife.dev.push");
		return true;
	}
	
	function InstallAgents(){
		CAgent::AddAgent(
		"\\Mlife\\Push\\ChanelsTable::agentDeleteDieChanels();",
		"mlife.push",
		"N",
		3600);
		return true;
	}
	
	function UnInstallAgents(){
		CAgent::RemoveAgent("\\Mlife\\Push\\ChanelsTable::agentDeleteDieChanels();", "mlife.push");
		return true;
	}
	
	function DoInstall()
	{
		global $USER, $APPLICATION;

		if ($USER->IsAdmin())
		{
			if ($this->InstallDB()){
				$this->InstallEvents();
				$this->InstallFiles();
				$this->InstallAgents();
			}
			$GLOBALS["errors"] = $this->errors;
		}
	}
	
	function DoUninstall()
	{
		global $DB, $USER, $DOCUMENT_ROOT, $APPLICATION, $step;
		if ($USER->IsAdmin())
		{
			if ($this->UnInstallDB()){
				$this->UnInstallEvents();
				$this->UnInstallFiles();
				$this->UnInstallAgents();
			}
		}
	}

}
?>