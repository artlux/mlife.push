<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
global $USER;
global $APPLICATION;

if(!CModule::IncludeModule('mlife.push')) {
		return;
}

$arResult = array();

$arResult["CHANEL"] = false;
if($USER->GetId()){
	$arResult["CHANEL"] = \Mlife\Push\ChanelsTable::getChanel($USER->GetId());
}

$this->IncludeComponentTemplate();

?>