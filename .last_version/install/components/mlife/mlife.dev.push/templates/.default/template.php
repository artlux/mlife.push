<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
global $APPLICATION;
$chanelId = $arResult["CHANEL"];
if($chanelId){
	CUtil::InitJSCore('jquery');
	$APPLICATION->AddHeadScript($templateFolder.'/js/pushstream.js');
}
?>
<script>
	window.curUrl = "<?=$APPLICATION->GetCurPage(false)?>";
	$(document).ready(function(){
		<?if($chanelId){?>
		setTimeout(function(){
			m_pushstream = new PushStream({
			  host: window.location.hostname,
			  port: window.location.port,
			  modes: "longpolling",
			  tagArgument: 'tag',
			  timeArgument: 'time',
			  useJSONP: true,
			  timeout: 30000,
			});
			m_pushstream.onmessage = window.m_messageReceivedPrivate;
			m_pushstream.addChannel('<?=$chanelId?>');
			m_pushstream.connect();
		},500);
		<?}?>
	});
	window.m_messageReceivedPrivate = function (text, id, channel) {
		m_MlifePortalUser_getContent(text);
	};
	window.m_MlifePortalUser_getContent = function(text){
	
		$.ajax({
			url: "/bitrix/tools/mlife.push/userdata.php",
			data: {text: text},
			dataType : "json",
			type: "POST",
			success: function (data, textStatus) {
				alert("view console, comand: "+data.mode);
				console.log(data);
			}
		});
		
	}
</script>