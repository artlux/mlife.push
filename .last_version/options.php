<?php
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.push
 * @copyright  2014 Zahalski Andrew
 */

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

CModule::IncludeModule("mlife.push");
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$POST_RIGHT = $APPLICATION->GetGroupRight("mlife.push");

if ($POST_RIGHT == "D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

?>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>

<?
$aTabs = array(
  array("DIV" => "edit1", "TAB" => Loc::getMessage("MLIFE_PUSH_OPT_TAB1"), "ICON"=>"main_user_edit", "TITLE"=>Loc::getMessage("MLIFE_PUSH_OPT_TAB1")),
);
$aTabs[] = array("DIV" => "edit4", "TAB" => GetMessage("MLIFE_PUSH_OPT_TAB4"), "ICON" => "vote_settings2", "TITLE" => GetMessage("MLIFE_PUSH_OPT_TAB4"));


if($REQUEST_METHOD == "POST" && $_REQUEST['Update']=="Y" && $POST_RIGHT=="W"){
	
	$url = trim($_REQUEST["url"]);
	\COption::SetOptionString("mlife.push", "url", $url);
	
}

$APPLICATION->SetTitle(Loc::getMessage("MLIFE_PUSH_OPT_PARAM_TITLE"));

?>

<form method="post" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=urlencode($mid)?>&amp;lang=<?=LANGUAGE_ID?>" id="options">
<?
$tabControl = new CAdminTabControl("tabControl", $aTabs,false,true);
$tabControl->Begin();
?>

<?
$tabControl->BeginNextTab();
?>
	<tr>
		<td width="40%"><?=Loc::getMessage("MLIFE_PUSH_OPT_PARAM_TITLE_4")?>:</td>
		<td><?$val = COption::GetOptionString("mlife.push", "url", "http://mysite.com/pub?id=#CHANEL_ID#");?>
			<input type="text" value="<?=$val?>" name="url" id="url">
		</td>
	</tr>
<?
$tabControl->BeginNextTab();
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights.php");
$tabControl->Buttons();
?>
	<input <?if ($MODULE_RIGHT<"W") echo "disabled" ?> type="submit" class="adm-btn-green" name="Update" value="<?=GetMessage("MLIFE_PUSH_OPT_SEND")?>" />
	<input type="hidden" name="Update" value="Y" />
<?$tabControl->End();?>
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>