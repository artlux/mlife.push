Модуль позволяет организовать мгновенные уведомления пользователю с помощью [nginx-push-stream-module](https://github.com/wandenberg/nginx-push-stream-module).
Предназначен для разработчиков, в инсталл включен демо-компонент (см. mlife.dev.push)...

**Требования:**
1. Установленный и настроенный nginx-push-stream-module
2. БУС версии 14.5.3+

Установить модуль можно из маркетплейс 1с-Битрикс - пока на модерации

**подписка пользователя**

```
#!php
<?$APPLICATION->IncludeComponent(
	"mlife:mlife.dev.push",
	"",
	Array(
	),
false
);?> 

```

**отправка уведомления, например, в админке с командной строки**

```
#!php
\Bitrix\Main\Loader::includeModule(mlife.push);
$arParams = array( "USER_ID" => 1, "CHAT_ID" => 1);
\Mlife\Push\ChanelsTable::sendMessageUser(
   $arParams["USER_ID"],
   array("module"=>"mlife.push","comand"=>"update_chat","chat"=>$arParams["CHAT_ID"])
);
```

**обработка команд**


```
#!php

$eventManager = \Bitrix\Main\EventManager::getInstance();
$eventManager->addEventHandler("mlife.push", "OnBeforeContentGet", "myPushOnBeforeContentGet");

function myPushOnBeforeContentGet(\Bitrix\Main\Event $event){
	
	$arData = $event->getParameter("data");
	
	$arReturn = array();

	if($arData['comand']=="update_chat" && intval($arData['chat'])>0){
		$arReturn["mode"] = "update_chat";
		$arReturn["id"] = intval($arData['chat']);
		$arReturn["url"] = "/personal/dialogs/".intval($arData['chat'])."/";
	}

	$result = new \Bitrix\Main\EventResult(1,$arReturn);
	return $result;

}
```